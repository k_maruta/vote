$(function(){
  routing(location.pathname)
})

function routing(path) {
  const route = [
    {path: '/login', class: Login, auth: false},
    {path: '/list', class: List, auth: true},
    {path: '/setting', class: Setting, auth: true},
    {path: '/vote', class: Vote, auth: false},
    {path: '/result', class: Result, auth: false},
  ]
  let target = route.filter(r => r.path === path)[0]
  new target.class(target.auth)
}

class Page {
  constructor(auth) {
    this.user = null    

    try {
      this.app = firebase.app();
      this.db = firebase.firestore();
      this.auth(auth, res => {
        if (res) {
          if (this.user) {
            this.setLogout()
          }
          this.show()
          this.init()
        } else {
          this.toPage('login')
        }
      })
    } catch (e) {
      console.error(e);
      $('#load').html('Error loading, check the console.')
    }
  }

  show() {
    $("#init").hide()
    $("#body").show()
  }

  loding() {
    $("#body").hide()
    $("#init").show()
  }

  auth(auth, callback) {
    if (!auth) {
      callback(true)
      return
    }
    let self = this
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        self.user = user
        callback(true)
      } else {
        callback(false)
      }
    })
  }

  toPage(path, newWindow) {
    if (newWindow) {
      open(`${this.host}/${path}`, "_blank" ) ;
    } else {
      location.href = `${this.host}/${path}`;
    }
  }

  setLogout() {
    let self = this
    $('#logout').on('click', () => {
      firebase.auth().signOut().then(() => {
        self.toPage('login')
      }).catch(function(error) {
        console.log(error)
      })
    })
  }

  get host() {
    return `${location.protocol}//${location.host}`
  }

  get query() {
    let url = location.search.substring(1)
    let q=url.split('&')
    let res = {}
    for (let v of q) {
      let [key, val] = v.split('=')
      res[key] = val
    }
    return res
  }

  dateFormat(dt, format = 'yyyy/mm/dd hh:MM:ss') {
    const fmt = {
        hh: (date) => `0${date.getHours()}`.slice(-2),
        h: (date) => date.getHours().toString(),
        MM: (date) => `0${date.getMinutes()}`.slice(-2),
        M: (date) => date.getMinutes().toString(),
        ss: (date) => `0${date.getSeconds()}`.slice(-2),
        dd: (date) => `0${date.getDate()}`.slice(-2),
        d: (date) => date.getDate().toString(),
        s: (date) => date.getSeconds().toString(),
        yyyy: (date) => date.getFullYear().toString(),
        yy: (date) => date.getFullYear().toString().slice(-2),
        mm: (date) => `0${date.getMonth() + 1}`.slice(-2),
        m: (date) => (date.getMonth() + 1).toString(),
    }
    let result = format
    for (let key of Object.keys(fmt)) {
        result = result.replace(key, fmt[key](dt))
    }
    return result
  }

}

class Login extends Page {
  constructor(auth) {
    super(auth)
  }

  init() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.toPage('list')        
      }
    });

    $('#login').on('click', () => {
      let id = $('#id').val()
      let pass = $('#password').val()
      this.login(id, pass)
    })
  }

  login(id, pass) {
    let self = this
    firebase.auth().signInWithEmailAndPassword(id, pass)
    .then((u) => {
      console.log(u)
    })
    .catch((error) => {
      self.message('Please check id or password.')
    })
  }

  message(str) {
    $("#message").html(str)
  }
}

class List extends Page {
  constructor(auth) {
    super(auth)
  }

  init() {
    this.view()

    $('#new').on('click', () => {
      this.toPage('setting')        
    })

    $(document).on('click', '.check', (ev) => {
      let elem = ev.currentTarget
      this.toPage(`setting?k=${$(elem).attr("key")}`)
    })

    $(document).on('click', '.vote', (ev) => {
      let elem = ev.currentTarget
      this.toPage(`vote?k=${$(elem).attr("key")}`, true)
    })

    $(document).on('click', '.result', (ev) => {
      let elem = ev.currentTarget
      this.toPage(`result?k=${$(elem).attr("key")}`, true)
    })

    $(document).on('click', '.del', (ev) => {
      if(window.confirm('really?')){
        let elem = ev.currentTarget
        this.delete($(elem).attr("key"))
      }
    })

  }

  view() {
    this.db.collection("vote")
    .orderBy("ts", "desc")
    .get()
    .then(snap => {
      if (!snap.empty) {
        let count = 0
        snap.forEach((v) => {
          let data = v.data()
          if (data.active) {
            this.addList(count++, v.id, data)
          }
        })
      }
    })
    .catch(error => {
      console.log(error)
      alert("error!")
    })
  }

  addList(idx, id, data) {
    let elem = $("#vote-list tr").eq(idx)
    elem.after([
      `<tr>`,
      `<td>${idx + 1}</td>`,
      `<td>${this.dateFormat(new Date(data.ts))}</td>`,
      `<td>${data.title}</td>`,
      `<td><div class="buttons">`,
      `<span class="button check is-primary" key="${id}">Check</span>`,
      `<span class="button vote is-warning" key="${id}">Vote</span>`,
      `<span class="button result is-warning" key="${id}">Result</span>`,
      `</div></td>`,
      `<td><div class="buttons">`,
      `<span class="button del is-danger is-outlined" key="${id}">Delete</span>`,
      `</div></td>`,
      `</tr>`
    ].join(''))
  }

  delete(key) {
    this.db.collection("vote").doc(key).update({
      active: false
    })
    .then(()=> {
      location.reload()
    })
  }
}

class Setting extends Page {
  constructor(auth) {
    super(auth)
  }

  init() {
    if (this.query.k) {
      this.view()
    }

    $('#back').on('click', () => {
      this.toPage('list')
    })

    $('#go').on('click', () => {
      this.toVote()
    })

    $(document).on('click', '.list-plus', (ev) => {
      this.plusList(ev.currentTarget)
    })

    $(document).on('click', '.list-minus', (ev) => {
      this.minusList(ev.currentTarget)
    })

    $(document).on('click', '.rank-plus', (ev) => {
      this.plusRank(ev.currentTarget)
    })

    $(document).on('click', '.rank-minus', (ev) => {
      this.minusRank(ev.currentTarget)
    })

    $('#create').on('click', () => {
      this.create()
    })

  }

  view() {
    $("#created").show()
    $("#url").html(`${this.host}/vote?k=${this.query.k}`)
    $("#qr").qrcode($("#url").html());

    this.db.collection("vote").doc(this.query.k).get()
    .then(snap => {
      let data = snap.data()
      if (!data || !data.active) {
        this.toPage("404")
        return
      } 
      this.setData(data)
    })
    .catch(error => {
      console.log(error)
      alert("error!")
    })
  }

  setData(data) {
    $("#title").val(data.title)
    for (let i in data.list) {
      let elem = $(".list").eq(i)
      if (elem.length === 0) {
        this.plusList($(".list-plus").eq(i - 1))
        elem = $(".list").eq(i)
      }
      elem.find(".list-name").val(data.list[i])
    }
    for (let i in data.rank) {
      let elem = $(".rank").eq(i)
      if (elem.length === 0) {
        this.plusRank($(".rank-plus").eq(i - 1))
        elem = $(".rank").eq(i)
      }
      elem.find(".rank-name").val(data.rank[i].name)
      elem.find(".rank-score").val(data.rank[i].score)
      elem.find(".rank-number").val(data.rank[i].number)
    }
    $("#nickname").attr("checked", data.nickname)
    $("#password").val(data.password)
    $("#create").remove()
  }
  plusList(elem) {
    $(elem).parents('.list')
    let listElem = $(elem).parents('.list')
    listElem.after(listElem[0].outerHTML)
    this.refreshListNo()
  }

  minusList(elem) {
    if ($('.list').length > 1) {
      $(elem).parents('.list').remove()
      this.refreshListNo()
    }  
  }

  plusRank(elem) {
    $(elem).parents('.rank')
    let listElem = $(elem).parents('.rank')
    listElem.after(listElem[0].outerHTML)
    let addElem = listElem.next()
    this.refreshRankNo()
    addElem.find(".rank-name").val(`No.${addElem.find(".rank-no").html()}`)
  }

  minusRank(elem) {
    if ($('.rank').length > 1) {
      $(elem).parents('.rank').remove()
      this.refreshRankNo()
    }  
  }

  refreshListNo() {
    $('.list-no').each((i, v) => {
      $(v).html(i + 1)
    })
  }

  refreshRankNo() {
    $('.rank-no').each((i, v) => {
      $(v).html(i + 1)
    })
  }

  create() {
    if (!this.validate()) {
      return
    }
    this.db.collection("vote").add(
      Object.assign(this.data, {ts: Date.now(), active: true}))
    .then((docRef) => {
      this.toPage(`setting?k=${docRef.id}`)
    })
    .catch((error) => {
      this.message("error, check the console.")
      console.log("Error adding document: ", error);
    })
  }

  validate() {
    this.message("")
    if (!this.data.title) {
      this.message("title is required.")
      return false
    }
    if (this.data.list.length < 2) {
      this.message("list is more than two are required.")
      return false
    }
    if (this.data.list.filter(v => !v).length > 0) {
      this.message("list is not entered.")
      return false
    }
    if (this.data.list.filter((v, i, self) => 
        self.indexOf(v) !== self.lastIndexOf(v)).length > 0) {
      this.message("list is duplicate.")
      return false
    }
    if (this.data.rank.filter(v => !v.name | !v.score | !v.number).length > 0) {
      this.message("rank is not entered.")
      return false
    }
    if (this.data.rank.map(v => v.name)
        .filter((v, i, self) => self.indexOf(v) !== self.lastIndexOf(v)).length > 0) {
      this.message("rank is duplicate.")
      return false
    }
    if (!this.data.password) {
      this.message("password is required.")
      return
    }
    return true
  }

  toVote() {
    this.toPage(`vote?k=${this.query.k}`, true)
  }

  get data() {
    return {
      title: $("#title").val(),
      list: (() => {
        let ar = []
        $(".list-name").each((i, v) => ar.push($(v).val()))
        return ar
      })(),
      rank: (() => {
        let ar = []
        $(".rank").each((i, v) => {
          ar.push({
            name: $(v).find(".rank-name").val(),
            score: $(v).find(".rank-score").val(),
            number: $(v).find(".rank-number").val()
          })
        })
        return ar
      })(),
      nickname: $("#nickname").is(":checked"),
      password: $("#password").val(),
      creator: this.user.email
    }
  }

  message(str) {
    $("#message").html(str)
  }

}

class Vote extends Page {
  constructor(auth) {
    super(auth)
    this.data = null
    this.votingNo = 0
    this.voted = {
      rank: []
    }
  }

  init() {
    this.loding()
    if (this.query.k) {
      this.view()
    } else {
      this.toPage("404")      
    }
  }

  view() {
    this.db.collection("vote").doc(this.query.k).get()
    .then(snap => {
      let data = snap.data()
      if (!data || !data.active) {
        this.toPage("404")
        return
      } 
      this.data = data
      this.show()
      $("#title").html(data.title)
      this.voting()
    })
    .catch(error => {
      console.log(error)
      alert("error!")
    })
  }

  voting() {
    $("#voting").show()
    $("#voting-message").html('')    
    if (this.votingNo === 0) {
      $("#voting-back").hide()
    } else {
      $("#voting-back").show()      
    }
    this.voted.rank[this.votingNo] = null
    let rank = this.data.rank[this.votingNo]
    $("#voting-name").html(rank.name)
    $("#voting-number").html(rank.number)
    $("#voting-list").html('')
    for (let i in this.data.list) {
      let name = this.data.list[i]
      let selected = false
      for(let r of this.voted.rank) {
        if (r && r[i]) {
          selected = true
          break
        }
      }
      $("#voting-list").append([
        `<div class="field" style="${selected ? 'display:none;' : ''}">`,
        `<p><span class="button">選択</span><span class="is-size-4" style="margin-left:10px;">${name}</span></p>`,
        '</div>'
      ].join(''))
    }
    $("#voting-list .button").on('click', (ev) => {
      let elem = ev.currentTarget
      if ($(elem).hasClass("is-danger")) {
        $(elem).removeClass("is-danger").next().removeClass("has-text-weight-bold")
      } else {
        $(elem).addClass("is-danger").next().addClass("has-text-weight-bold")
      }
    })

    $("#voting-back").off('click').on('click', (ev) => {
      this.votingNo--
      this.voting()
    })

    $("#voting-next").off('click').on('click', (ev) => {
      $("#voting-message").html('')
      let res = {}
      $("#voting-list .button").each((i, v) => {
        if ($(v).hasClass("is-danger")) {
          res[i] = true
        }
      })
      let count = Object.keys(res).length
      if (Number(rank.number) > count) {
        $("#voting-message").html(`${rank.number}人選んでください`)
        return
      }
      if (Number(rank.number) < count) {
        $("#voting-message").html(`${rank.number}人だけ選んでください`)
        return
      }
      this.voted.rank[this.votingNo] = res
      this.votingNo++
      if (this.data.rank.length <= this.votingNo) {
        $("#voting").hide()
        if (this.data.nickname) {
          this.naming()
        } else {
          this.checking()
        }
      } else {
        this.voting()
      }
    })
  }

  naming() {
    $("#naming").show()    
    $("#naming-message").html('')    
    this.voted.nickname = null

    $("#naming-back").off('click').on('click', (ev) => {
      $("#naming").hide()
      this.votingNo--      
      this.voting()
    })

    $("#naming-next").off('click').on('click', (ev) => {
      $("#voting-message").html('')
      let nickname = $("#naming-name").val()
      if (!nickname) {
        $("#naming-message").html(`ニックネームを入力してください`)
        return
      }
      this.db.collection("vote").doc(this.query.k)
        .collection('result').where('nickname','==', nickname).get()
        .then(s => {
          if (s.empty) {
            this.voted.nickname = nickname
            $("#naming").hide()
            this.checking()      
          } else {
            $("#naming-message").html(`すでに登録されています`)            
          }
        })

    })
  }

  checking() {
    $("#checking").show()
    $("#checking-message").html('')        
    $("#checking-list").html('')
    for (let i in this.voted.rank) {
      let no = this.voted.rank[i]
      for (let j of Object.keys(no)) {
        $("#checking-list").append([
          `<p class="is-size-5"><span>${this.data.rank[i].name}： ${this.data.list[j]}</span></p>`,
        ].join(''))
      }
    }
    if (this.voted.nickname) {
      $("#checking-name-area").show()
      $("#checking-name").html(this.voted.nickname)
    } else {
      $("#checking-name-area").hide()
    }

    $("#checking-back").off('click').on('click', (ev) => {
      $("#checking").hide()
      if (this.data.nickname) {
        this.naming()
      } else {
        this.votingNo--        
        this.voting()
      }
    })

    $("#checking-next").off('click').on('click', (ev) => {
      $("#checking").hide()
      this.voted.ts = Date.now()
      this.db.collection("vote").doc(this.query.k)
        .collection('result').add(this.voted)
        .then((docRef) => {
          $("#checking").hide()
          this.finish()
        })
        .catch((error) => {
          $("#checking-message").html("error, check the console.")
          console.log("Error adding document: ", error);
        })  
    })
  }

  finish() {
    $("#finish").show()

    let timer = setTimeout(()=> {
      location.reload()
    }, 10000)
    $("#finish-next").off('click').on('click', (ev) => {
      clearTimeout(timer)
      location.reload()
    })

  }
}

class Result extends Page {
  constructor(auth) {
    super(auth)
    this.data = null
    this.records = null
  }

  init() {
    this.loding()
    if (this.query.k) {
      this.view()
    } else {
      this.toPage("404")      
    }
  }

  view() {
    this.db.collection("vote").doc(this.query.k).get()
    .then(snap => {
      let data = snap.data()
      if (!data || !data.active) {
        this.toPage("404")
        return
      } 
      this.data = data
      this.show()
      $("#title").html(data.title)

      $("#pass-check").on('click', (ev) => {
        if ($("#password").val() === this.data.password) {
          $("#pass-area").hide()
          this.result()
        } else {
          $("#pass-message").html("パスワードが正しくありません")
        }
      })

      $("#update").on('click', (ev) => {
        this.result(true)
      })

    })
    .catch(error => {
      console.log(error)
      alert("error!")
    })
  }

  result(isUpdate) {
    $("#result-list").show()
    $(".result-body").remove()
    this.loding()
    this.db.collection("vote").doc(this.query.k)
    .collection("result").get()
    .then(snap => {
      if (snap.empty) {
        this.records = []
      } else {
        this.records = snap.docs.map(doc => doc.data())
      }
      let sum = this.summary()
      $("#count").html(this.records.length)
      if (!isUpdate) {
        for (let r of this.data.rank) {
          $("#result-head").append([
            `<th>${r.name} (件数)</th>`
          ])
        }
      }
      for (let data of sum) {
        let elem = $("#result-list tr")
        let last = elem.eq(elem.length - 1)
        last.after([
          `<tr class="result-body">`,
          `<td>${data.rank}</td>`,
          `<td>${data.no}</td>`,
          `<td>${data.name}</td>`,
          `<td>${data.score}</td>`,
          ...data.ranks.map(v => `<td>${v}</td>`),
          `</tr>`
        ].join(''))
      }
      $("#voted-list").show()
      $(".voted-body").remove()
      for (let i in this.records) {
        let data = this.records[i]
        let elem = $("#voted-list tr")
        let last = elem.eq(elem.length - 1)
        last.after([
          `<tr class="voted-body">`,
          `<td>${Number(i) + 1}</td>`,
          `<td>${data.nickname || 'unknown'}</td>`,
          `<td>${this.dateFormat(new Date(data.ts))}</td>`,
          `</tr>`
        ].join(''))
      }
      this.show()
    })
  }

  summary() {
    let sum = this.data.list.map((v, i) => {
      let ranks = this.data.rank.map(v => 0)
      for (let rec of this.records) {
        for (let k in rec.rank) {
          if (rec.rank[k][i]) {
            ranks[k]++
          }
        }
      }
      let score = ranks.reduce((t, c, i) => {
        return t + c * this.data.rank[i].score
      }, 0)
      return {
        no: i + 1,
        name: v,
        score: score,
        ranks: ranks
      }
    })
    sum.sort((a,b) => {
      if (a.score > b.score ) return -1
      if (a.score < b.score ) return 1
      for (let k in a.ranks) {
        if (a.ranks[k] > b.ranks[k]) return -1
        if (a.ranks[k] < b.ranks[k]) return 1
      }
      if (a.no > b.no) return 1
      if (a.no < b.no) return -1
    return 0;
    })
    for(let i in sum) {
      i =  Number(i)
      if (i > 0) {
        let data1 = sum[i]
        let data2 = sum[i -1]
        if (data1.score === data2.score
        && JSON.stringify(data1.ranks) === JSON.stringify(data2.ranks)) {
          data1.rank = data2.rank 
        } else {
          data1.rank = i + 1
        }
      } else {
        sum[i].rank = i + 1
      }
    }
    return sum
  }

}
